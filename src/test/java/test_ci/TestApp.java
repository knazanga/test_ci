package test_ci;

import static org.junit.Assert.*;

import org.junit.Test;

import tests.patterns.bridge.MessageFactory;
import tests.patterns.bridge.MessageHandler;
import tests.patterns.bridge.MyMessage;
import tests.patterns.bridge.UnsupportedMessageException;
import tests.patterns.bridge.UntouchableMessageB;

public class TestApp {

	@Test
	public void testPatternBrige() {
		try {
			MessageHandler messageHandler = new MessageHandler();
			MessageFactory messageFactory = new MessageFactory();

			UntouchableMessageB incomingMessage = new UntouchableMessageB();
			MyMessage instanciatedMessage = messageFactory.createMyMessageInstance(incomingMessage);

			assertEquals(messageHandler.handleMessage(instanciatedMessage), "I am message B");

		} catch (UnsupportedMessageException e) {
			e.printStackTrace();
		}
	}

}
