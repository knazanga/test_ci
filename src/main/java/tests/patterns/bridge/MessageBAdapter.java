package tests.patterns.bridge;

public class MessageBAdapter implements MyMessage {
	private final UntouchableMessageB messageB;

	public MessageBAdapter(UntouchableMessageB incomingMessage) {
		this.messageB = incomingMessage;
	}

	public String makeAction() {
		return messageB.messageBOperation();
	}

}
