package tests.patterns.bridge;

public class UntouchableMessageA {

	public String messageAOperation() {
		return "I am message A";
	}
}
