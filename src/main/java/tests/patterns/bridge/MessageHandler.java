package tests.patterns.bridge;

public class MessageHandler {
	
	public String handleMessage(MyMessage message){
		return message.makeAction();
	}
	
}
