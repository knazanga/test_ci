package tests.patterns.bridge;

public class MessageCAdapter implements MyMessage {
	private final UntouchableMessageC messageC;

	public MessageCAdapter(UntouchableMessageC incomingMessage) {
		this.messageC = incomingMessage;
	}

	public String makeAction() {
		return messageC.messageCOperation();
	}
}
