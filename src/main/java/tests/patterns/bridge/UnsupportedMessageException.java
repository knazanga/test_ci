package tests.patterns.bridge;

public class UnsupportedMessageException extends Exception {
	public UnsupportedMessageException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
