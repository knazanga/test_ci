package tests.patterns.bridge;

public class MessageAAdapter implements MyMessage {
	private final UntouchableMessageA realMessageA;

	public MessageAAdapter(UntouchableMessageA incomingMessage) {
		this.realMessageA = incomingMessage;
	}

	public String makeAction() {
		return realMessageA.messageAOperation();
	}

}
