package tests.patterns.bridge;

public class MessageFactory {
	
	public MyMessage createMyMessageInstance(Object incomingMessage) throws UnsupportedMessageException{
		if(incomingMessage instanceof UntouchableMessageA){
			return new MessageAAdapter((UntouchableMessageA)incomingMessage);
		}else if(incomingMessage instanceof UntouchableMessageB){
			return new MessageBAdapter((UntouchableMessageB)incomingMessage);
		}else if(incomingMessage instanceof UntouchableMessageC){
			return new MessageCAdapter((UntouchableMessageC)incomingMessage);
		}
		throw new UnsupportedMessageException("Impossible de traiter ce type de message");
	}

}
