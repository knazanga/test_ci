package tests.patterns.bridge;

public class UntouchableMessageB {
	public String messageBOperation() {
		return "I am message B";
	}
}
