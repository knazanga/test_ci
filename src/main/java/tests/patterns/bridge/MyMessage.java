package tests.patterns.bridge;

public interface MyMessage {
	
	public String makeAction();

}
